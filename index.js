/* export mongoose schemas and service class */
var mongoose = require("mongoose");
module.exports = {

	schema:{
		Integration:require("./models/Integration"),
		TagPoints:require("./models/TagPoints"),
		UserPoints:require("./models/UserPoints"),
		UrlPoints:require("./models/UrlPoints"),
		PointsAllocation:require("./models/PointsAllocation")
	},

	service:{
		Service:require("./services/Service")
	},

	mongoose:mongoose
};