mongoose = require("mongoose");

var PointsAllocation = new mongoose.Schema({

	//parent integration
	integration_id:{
		type:mongoose.Schema.Types.ObjectId,
		required:true
	},

	//User who allocated the points (username)
	to:{
		type:String,
		required:true
	},

	//User receiving points (username)
	from:{
		type:String,
		required:true,
	},

	//entire message
	body:{
		type:String,
		required:true
	},

	//all mentions
	mentions:{
		type:Array,
		required:true
	},
	
	//all tags
	tags:{
		type:Array,
		required:false
	},

	//room where allocation occured
	room_id:{
		type:String,
		required:true
	},
	
	//name of the room
	room_name:{
		type:String,
		required:true
	},

	created:Date
});
module.exports = mongoose.model("PointsAllocation", PointsAllocation);