/* TagPoints represent points for a given hashtag */
var mongoose = require("mongoose");
var TagPoints = new mongoose.Schema({
	
	name:{
		type:String,
		required:true
	},

	total:Number,
	week:Number,
	day:Number,
	month:Number

});

module.exports = mongoose.model("TagPoints", TagPoints);