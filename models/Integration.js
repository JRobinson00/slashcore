mongoose = require("mongoose");

var Integration = new mongoose.Schema({
	
	account_id:{
		type:mongoose.Schema.Types.ObjectId,
		required:true
	},

	/* slack, hipchat, irc, etc */
	name:{
		type:String,
		required:true
	},

	created:Date,
	modified:Date,

	/* generic object for integration specific credentials. (tokens and stuff) */
	credentials:{
		type:Object,
		required:true
	}

});

module.exports = mongoose.model("Integration", Integration);
