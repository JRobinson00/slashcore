/* UrlPoints represent points for a given url */
var mongoose = require("mongoose");
var UrlPoints = new mongoose.Schema({
	
	//the url is the name?
	name:{
		type:String,
		required:true
	},
	//TOOD: set defaults of 0?
	total:Number,
	week:Number,
	day:Number,
	month:Number,

	created:Date

});

module.exports = mongoose.model("UrlPoints", UrlPoints);