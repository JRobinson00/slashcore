/* UserPoints denote points for a user on a team */

var mongoose = require("mongoose");
var UserPoints = new mongoose.Schema({

	//JR: changed this to "name" to more closely match "TagPoints" Schema
	name:{
		type:String,
		required:true
	},

	/* integration id of the integration that owns us, if there is one. otherwise we're global */
	team_id:{
		type:mongoose.Schema.Types.ObjectId,
		required:false,
		default:null
	},
	
	total:Number,//lifetime total
	week:Number,//this week's total
	day:Number,//today
	month:Number//this month

});

module.exports = mongoose.model("UserPoints", UserPoints);