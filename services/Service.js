//var mongoose = require("mongoose");
var _ = require("underscore");

//constructor
function Service(model) {
	if(!model) return new Error("A Service requires a model!");
	this.model = model;
};

//override in sub classes to exclude/include properties.
//convert mongo '_id' to backbone-friendly 'id'.
Service.prototype.sanitizeModel = function(dirtyModel) {
	//console.log("sanitizing model...", dirtyModel);
	var m = {};
	if(dirtyModel === null || dirtyModel === undefined) return null;
	_.extend(m, dirtyModel.toJSON());
	m.id = String(dirtyModel._id);
	//delete props we don't need.
	delete m._id;
	delete m.__v;
	delete m.account_id;//?do we want to strip account_id here?
	delete m.password;//strip passwords
	return m;
};

//return a collection
Service.prototype.list = function(query, cb) {
	console.log("service.list:", query, this.model);
	var self = this;
	this.model.find(query, function(err, models) {
		if(err) return cb(err);
		var r = [];
		for(var i=0; i<models.length; i++) {
			var s = self.sanitizeModel(models[i]);
			r.push(s);
		}
		return cb(null, r);
	});
};

//
Service.prototype.find = function(query, cb) {
	var self = this;
	this.model.findOne(query, function(err, m){
		//console.log("attempted to find...", query, "found:", m);
		if(err) return cb(err);
		return cb(null, self.sanitizeModel(m));
	});
};

//search 
Service.prototype.search = function(query, cb) {
	var self = this;
	this.model.find(query, function(err, models){
		if(err) return cb(err);
		var r = [];
		for(var i=0; i<models.length; i++) {
			var s = self.sanitizeModel(models[i]);
			r.push(s);
		}
		return cb(null, r);
	})
};

//handle creating a model, or creating a unique model
//can be called like:
//service.create(props:{}, cb:function) to create a model
//or
//service.craeat(props:{}, unique=true, uniqueProps:{}, cb:function)
//TODO: use https://www.npmjs.com/package/mongoose-unique-validator
//in Schema's instead of dealing with unique here, and at the router
//level
Service.prototype.create = function(props, unique, uniqueProps, cb) {
	var self = this;
	var newModel;
	if(typeof unique == "function") {
		//just create it. It doesn't have to be unique.
		cb = unique;
		newModel = new this.model();
		_.extend(newModel, props);
		newModel.save(function(err, m) {
			if(err) return cb(err);
			return cb(null, self.sanitizeModel(m));
		})
	} else {
		//create a unique model.
		this.model.findOne(uniqueProps, function(err, model) {
			if(err) return cb(err);
			if (model) return cb({message:"model is not unique. not created"});
			//it's unique! go ahead and create it
			newModel = new self.model();
			_.extend(newModel, props);
			newModel.save(function(err) {
				if (err) return cb(err);
				console.log("Calling cb with newModel...", newModel.toJSON());
				return cb(null, self.sanitizeModel(newModel));
			});
		});
	}
};

//insert an array of items into the collection all at once.
Service.prototype.createMany = function(resources, cb) {
	var self = this;
	this.model.create(resources, function(err, models){
		if(err) return cb(err);
		var results = [];
		for(var i=0; i<models.length; i++) {
			results.push(this.sanitizeModel(models[i]))
		}
		return cb(null, results);
	});
};

//update a given model
Service.prototype.update = function(query, props, cb) {
	var self = this;
	this.model.findOneAndUpdate(query, props, function(err, m) {
		if(err) return cb(err);
		return cb(null, self.sanitizeModel(m));
	});
};

//update or create a given model
Service.prototype.updateOrCreate = function(query, props, cb) {
	var self = this;
	//try to update first
	this.update(query, props, function(err, model) {
		if(err) return cb(err);
		if(model == null) {
			//model not found, create!
			self.create(props, function(err, model){
				if(err) return cb(err);
				return cb(null, model);
			})
		} else {
			//model updated
			cb(null, model);
		}
	})	
};

//delete a given model
Service.prototype.delete = function(query, cb) {
	var self = this;
	this.model.remove(query, function(err) {
		if(err) return cb(err);
		return cb(null);
	});
};

//
module.exports = Service;
